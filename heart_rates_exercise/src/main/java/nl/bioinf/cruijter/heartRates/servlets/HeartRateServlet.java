package nl.bioinf.cruijter.heartRates.servlets;

import nl.bioinf.cruijter.heartRates.config.WebConfig;
import nl.bioinf.cruijter.heartRates.model.ZonesCalculator;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

@WebServlet(name = "HeartRateServlet", urlPatterns = "/heart-rate-zones")
public class HeartRateServlet extends HttpServlet {
    TemplateEngine templateEngine = new TemplateEngine();

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        String maxHeartRate = request.getParameter("max_heart_rate");
        int max = Integer.parseInt(maxHeartRate);

        ZonesCalculator zonesCalculator = new ZonesCalculator();
        List<Integer> zones = zonesCalculator.getZones(max);

        ctx.setVariable("zone50", zones.get(0));
        ctx.setVariable("zone60", zones.get(1));
        ctx.setVariable("zone70", zones.get(2));
        ctx.setVariable("zone80", zones.get(3));
        ctx.setVariable("zone90", zones.get(4));
        ctx.setVariable("zone100", zones.get(5));

        templateEngine.process("zones", ctx, response.getWriter());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        templateEngine.process("get_heart_rate", ctx, response.getWriter());
    }
}
