package nl.bioinf.cruijter.heartRates.model;

import java.util.ArrayList;
import java.util.List;

public class ZonesCalculator {

    public ZonesCalculator() {
    }

    public List<Integer> getZones(int maxHeartRate) {
        List<Integer> zones = new ArrayList<>();

        zones.add((int) (maxHeartRate * 0.5));
        zones.add((int) (maxHeartRate * 0.6));
        zones.add((int) (maxHeartRate * 0.7));
        zones.add((int) (maxHeartRate * 0.8));
        zones.add((int) (maxHeartRate * 0.9));
        zones.add((int) (maxHeartRate * 1.0));

        return zones;
    }

}
