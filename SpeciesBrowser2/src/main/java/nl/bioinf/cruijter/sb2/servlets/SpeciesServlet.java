package nl.bioinf.cruijter.sb2.servlets;

import nl.bioinf.cruijter.sb2.CSVReader;
import nl.bioinf.cruijter.sb2.EagleMaker;
import nl.bioinf.cruijter.sb2.config.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static nl.bioinf.cruijter.sb2.config.WebConfig.*;

@WebServlet(name = "SpeciesServlet", urlPatterns = { "/give.species", "/give.eagle"} )
public class SpeciesServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    List<String> history = new ArrayList<>();


    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = createTemplateEngine(servletContext);
        servletContext.getContextPath();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String eagleSpecies = request.getParameter("eagle_species");

        String fileName = getServletContext().getInitParameter("species_data");
        System.out.println(fileName);
        final Map<String, EagleMaker> eagles = CSVReader.fileReader(fileName);
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        String uri = request.getRequestURI();
        if (uri.endsWith("/give.species")) {
            HttpSession session = request.getSession();

            if (history.size() > 5) {
                history.remove(0);
            }
            System.out.println("hello");

            session.setAttribute("history", history);
            ctx.setVariable("eagles", eagles);
            templateEngine.process("species", ctx, response.getWriter());
        } else if (uri.endsWith("/give.eagle")) {
            history.add(eagleSpecies);
            ctx.setVariable("eagle", eagles.get(eagleSpecies));
            ctx.setVariable("eagle_name", eagles.get(eagleSpecies).getEnglishName());
            ctx.setVariable("dutchname", eagles.get(eagleSpecies).getDutchName());
            ctx.setVariable("picture", eagles.get(eagleSpecies).getPicture());
            templateEngine.process("eagle", ctx, response.getWriter());
        }
    }


}

