package nl.bioinf.cruijter.sb2;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class CSVReader {

    public static Map<String, EagleMaker> fileReader(String file) {
//        Path path = Paths.get("/Users/carli/OneDrive/Documenten/BIN3/blok2/WIS/new_wis/wis2/SpeciesBrowser2/data/species.csv");
        Path path = Paths.get(file);
        Map<String, EagleMaker> eagles = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                if(lineNumber == 1) continue;

                String[] elements =  line.split(";");

                String scientificName = elements[0];
                String englishName = elements[1];
                String dutchName = elements[2];
                double wingSpan = Double.parseDouble(elements[3]);
                String picture = elements[4];

                EagleMaker eagle = new EagleMaker(scientificName, englishName, dutchName, wingSpan, picture);

                eagles.put(eagle.scientificName, eagle);

            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return eagles;
    }

}

