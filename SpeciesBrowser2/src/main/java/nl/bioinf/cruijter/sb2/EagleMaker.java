package nl.bioinf.cruijter.sb2;

public class EagleMaker {
    String scientificName;
    String englishName;
    String dutchName;
    double wingSpan;
    String picture;


    public EagleMaker(String scientificName, String englishName, String dutchName, double wingSpan, String picture){
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.wingSpan = wingSpan;
        this.picture = picture;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public double getWingSpan() {
        return wingSpan;
    }

    public String getPicture() {
        return picture;
    }


}
