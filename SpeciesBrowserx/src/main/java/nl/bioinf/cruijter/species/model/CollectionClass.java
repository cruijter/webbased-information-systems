package nl.bioinf.cruijter.species.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionClass {

    public CollectionClass(){}

    public Map<String, Eagle> makeMap(List<String> data) {
        Map<String, Eagle> eagles = new HashMap<>();
        String[] keys = {"steppe", "american", "chaco", "crested", "philippine"};
        for (int i = 0; i < data.size(); i++) {
            String[] elements = data.get(i).split(";");
            String scientificName = elements[0];
            String englishName = elements[1];
            String dutchName = elements[2];
            Double wingSpan = Double.parseDouble(elements[3]);
            String picture = elements[4];

            Eagle eagle = new Eagle(scientificName, englishName, dutchName, wingSpan, picture);

            eagles.put(keys[i], eagle);
        }
        return eagles;
    }

}
