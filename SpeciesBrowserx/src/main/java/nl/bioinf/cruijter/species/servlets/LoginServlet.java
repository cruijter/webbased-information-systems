package nl.bioinf.cruijter.species.servlets;

import nl.bioinf.cruijter.species.config.WebConfig;
import nl.bioinf.cruijter.species.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                response.getLocale());

        HttpSession session = request.getSession();

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (session.isNew() || session.getAttribute("user") == null) {
            boolean authenticated = authenticate(username, password);

            if (authenticated) {
                session.setAttribute("user", new User(username, password));
                response.sendRedirect("/home");
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect, please try again.");
                templateEngine.process("login", ctx, response.getWriter());
            }
        } else {
            response.sendRedirect("/home");
        }
    }

    private boolean authenticate(String name, String password) {
        boolean authenticated = false;

        List<User> users = User.getSome();
        for (User user : users) {
            if (name.equals(user.getName()) && password.equals(user.getPassword())) {
                authenticated = true;
                break;
            }
        }

        return authenticated;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("message", "Fill in your username and password");

        templateEngine.process("login", ctx, response.getWriter());
    }
}
