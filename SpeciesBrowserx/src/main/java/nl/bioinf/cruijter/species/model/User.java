package nl.bioinf.cruijter.species.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String password;
    private Role role;

    public enum Role {
        ADMIN,
        USER,
        GUEST
    }

    public User() {
    }

    public User(String name, Role role) {
        this.name = name;
        this.role = role;
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String getName() {
        return this.name;
    }

    public static List<User> getSome() {
        List<User> users = new ArrayList<>();
        User u;
        u = new User("Hank", "hank", Role.ADMIN);
        users.add(u);
        u = new User("Roger", "roger", Role.USER);
        users.add(u);
        u = new User("Diana", "diana", Role.GUEST);
        users.add(u);

        return users;
    }


    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
