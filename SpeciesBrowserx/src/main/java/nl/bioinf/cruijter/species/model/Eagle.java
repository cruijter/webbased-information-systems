package nl.bioinf.cruijter.species.model;

public class Eagle {
    String scientificName;
    String englishName;
    String dutchName;
    double wingSpan;
    String image;

    public Eagle(String scientificName, String englishName, String dutchName, double wingSpan, String image) {
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.wingSpan = wingSpan;
        this.image = image;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public void setDutchName(String dutchName) {
        this.dutchName = dutchName;
    }

    public double getWingSpan() {
        return wingSpan;
    }

    public void setWingSpan(double wingSpan) {
        this.wingSpan = wingSpan;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
