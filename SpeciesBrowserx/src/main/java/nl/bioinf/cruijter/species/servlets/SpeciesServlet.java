package nl.bioinf.cruijter.species.servlets;

import nl.bioinf.cruijter.species.config.WebConfig;
import nl.bioinf.cruijter.species.model.CSVReader;
import nl.bioinf.cruijter.species.model.CollectionClass;
import nl.bioinf.cruijter.species.model.Eagle;
import nl.bioinf.cruijter.species.model.HistoryMaker;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet(name = "SpeciesServlet", urlPatterns = {"/home", "/give.details"})
public class SpeciesServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    List<String> history = new ArrayList<>();


    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        String file = getServletContext().getInitParameter("data_folder");

        CollectionClass collectionClass = new CollectionClass();
        CSVReader reader = new CSVReader();

        List<String> data = reader.fileReader(file);
        Map<String, Eagle> eagles = collectionClass.makeMap(data);

        String eagleName = request.getParameter("eagle_species");
        Eagle eagle = eagles.get(eagleName);

        HistoryMaker historyMaker = new HistoryMaker();

        HttpSession session = request.getSession();

        String uri = request.getRequestURI();
        if(uri.endsWith("home")) {
            ctx.setVariable("eagles", eagles);
            templateEngine.process("species-listing", ctx, response.getWriter());
        } else if (uri.endsWith("give.details")) {
            historyMaker.addToList(history, eagleName);
            ctx.setVariable("eagle_name", eagleName);
            ctx.setVariable("eagle", eagle);
            templateEngine.process("species", ctx, response.getWriter());
        }
        session.setAttribute("history", history);

    }
}
