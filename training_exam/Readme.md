## Test header (tentamen voorblad)

- **Teacher** Michiel Noback (NOMI), to be reached at +31 50 595 4691
- **Scanned by** KEMC
- **Test size** 10 assignments (with subquestions)
- **Aiding materials** Computer on the BIN network
- **Supplementary materials**  
    - The Java gitbook, on your Desktop
    
- **TO BE SUBMITTED  
This project, as `zip` archive. Use this name for the zip:  
 `JavaAssessment<YOURNAME>-<YOURSTUDENTNUMBER>.zip`**

## Instructions
On your Desktop you will find the assessment as a Gradle-managed Java project, as well as the submit script `submit_your_work`. 

First start IntelliJ in evaluation mode:  

1. Start IntelliJ
2. Choose "Do not import settings"
3. Accept licence (first scroll down)
4. Choose "Don't send statistics"
5. Choose "Skip remaining and set defaults" (or customize at the cost of assessment time...)
6. Check the box "Evaluate for free" and click "Activate"
7. Choose "Open" in "Welcome to IntelliJ" window
8. Navigate to `~/Desktop/JavaAssessment…/` and select the assessment project folder
9. Since this project requires Java >10, it may be necessary to specify the JDK (via File -> Project Structure); it is located at `/usr/lib/jvm/java-11-openjdk-amd64/`. If this location is accidentally outdated try `echo $JAVA_HOME` to find it.
 

Next, deal with the assignments. 
After finishing, create a zip named `JavaAssessment<YOURNAME>-<YOURSTUDENTNUMBER>.zip` (using the terminal `zip` command) and submit it using the submit script. 
Type `submit_your_work --help` in the terminal to get help on its usage.

The possible number of points to be scored are indicated for all assignments. 
Your grade will be calculated as  
`double grade = (total/maximumPoints * 10) < 1 ? 1 : (total/maximumPoints * 10);`

**_Do not submit code that does not compile - debugging is not part of the grading process!_**

## The Assignments

These assignments revolve around the creation of a simple random sequence generator service for Protein and DNA.
Folder `src/main` is the base folder, where you will find Java class and other files that may be referred to from within the assignments. Some boilerplate code has already been provided in class `WebConfig`.

You may create main() methods wherever you think they help you implement functionality. Alternatively, you may create 
test classes and -methods using JUnit.


#### Assignment 1: A fragments.html page (15 points)

Create a Thymeleaf page that can serve as a container for several fragments to be reused in this application.
This `fragments.html` page should have these fragments:

1. A banner displaying the image `banner_s.jpg` from the folder `graphics`, and displaying some page header text and 
a subtitle that says "You are looking at the <SEQUENCE REQUEST FORM/SEQUENCE VIEW PAGE>." where the text between the 
diamond operators should be passed as a parameter to the fragment. 

2. Two fragments that can be conditionally included, based on the sequence type that was requested. One fragment should show the table of DNA ambiguities (`dna_ambiguities.png`). The other fragment should display the Amino Acid Codes (`amino_acids.png`). Both should have some explaining text as well.

#### Assignment 2: An internationalized request form (20 points)

Create an Thymeleaf page called `sequence_request_form.html`.
Include the banner from Assignment 1 and pass it the correct text for the placeholder.
Next, create a simple web form that a user can use to request a random sequence.

This form should include four elements
- The first element should be a pull-down menu offering the choice of sequence type, DNA or RNA, with a default value of DNA with the name `seq_type`. 
- The second element should be a text field indicating the requested length of the sequence with the name `seq_length`.  
- The third element should be a text field indicating the requested number of sequences with the name `seq_number`.  
- The fourth element should be a submit button for the form.  

Except for the banner, all text in this page should be made internationalized through a supporting resource bundle.  

Upon submitting, a POST request should be sent to the url `/create.sequence`. Handling this request will be dealt with in the next question.


#### Assignment 3: Servlet Request handling (15 points)

*Assignment 3a (2 points)*
Create a servlet called `Servlet` serving the url `/create.sequence`. 

*Assignment 3b (3 points)*
In this Servlet, GET request should simply serve the page `sequence_request_form.html` from the previous assignment.

*Assignment 3a (10 points)*
In the same Servlet, POST requests should deal with the request form of the previous assignment (2).
The request parameters `seq_type`, `seq_number` and `seq_length` should be checked for validity: the type of sequence 
should be "dna" or "protein", and the sequence length and number should be a positive integer with a maximum of 500. 
If these are not valid, the method 
should serve the request form again with an appropriate error message to be displayed in that page. 
Adjust this page to achieve this (internationalization is not required).

Processing correct values will be dealt with in the next assignments.

If you failed at assignment 2, you can "simulate" a request by defining two variables for these two properties.

#### Assignment 4: Creating the model (10 + 5 points)

Create a regular Java class called `RandomSequenceCreator` and within that class create a 
static utility method called `createSequences` that accepts a sequence type, a length and a number parameter. 
A List of random sequences should be created and returned.
Hint: use something like this:  

```java
private static final String[] nucleotides = {"A", "C", "G", "T"};
private static final String[] aminoAcids = {"A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"};
```
and

```java
String character = nucleotides[(int)(Math.random() * characters.length)];
```

For a correct but inefficient solution, you get 10 points. For an efficient and well-though solution you get 5 bonus points.
Within the Servlet method handling the post request, invoke the model created in the last assignment and pass the created sequences to the next Thymeleaf template, `sequence_server.html`. 

If you fail here, simply pass a hardcoded String list of DNA or Protein sequences.


#### Assignment 5: Displaying results (15 points)

Create a listing of the created sequences in page `sequence_server.html`, in Fasta format. 
The generated sequences should look something like 

```
>Random <DNA/PROTEIN> number <NUMBER>
<SEQUENCE>
```

Note that the pointy brackets `<>` are **_placeholders_**, not literals!
Put the banner at the top of this page as well.


#### Assignment 4: A session history (20 points)

Modify your application so that a session history is maintained. This session history should be listed 
in the `sequence_request_form.html` page. The listing should hold information on the last 5 requests, showing (a) 
the type, (b) the length and (c) the number of sequences generated. In order to do this nicely you should start by creating a 
`RequestHistory` class.
 



That's it.