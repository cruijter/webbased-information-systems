package nl.bioinf.wis;

import java.util.List;

public class RequestHistory {
    private int length;
    private int number;
    private String type;

    public RequestHistory(){}

    public RequestHistory(int length, int number, String type) {
        this.length = length;
        this.number = number;
        this.type = type;
    }

    public int getLength() {
        return this.length;
    }

    public int getNumber() {
        return this.number;
    }

    public String getType() {
        return this.type;
    }

//    public List<String> makeList(List<String> history, String seqType, int seqLength, int seqNumber) {
//
//        if (history.size() >= 5) {
//            history.remove(0);
//        }
//        history.add("The type chosen: " + seqType + "\n" + "The length chosen: " + seqLength + "\n" + "The chosen number of sequences: " + seqNumber);
//        return history;
//    }
}
