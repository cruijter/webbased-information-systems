package nl.bioinf.wis.servlets;

import nl.bioinf.wis.RandomSequenceCreator;
import nl.bioinf.wis.RequestHistory;
import nl.bioinf.wis.WebConfig;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = "/create.sequence")
public class Servlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        String seqType = request.getParameter("seq_type");
        int seqLength = Integer.parseInt(request.getParameter("seq_length"));
        int seqNumber = Integer.parseInt(request.getParameter("seq_number"));


        HttpSession session = request.getSession();
        if (session.isNew()) {
            session.setAttribute("history", new ArrayList<RequestHistory>());
        }

        List<RequestHistory> requestHistory = (List)session.getAttribute("history");
        requestHistory.add(new RequestHistory(seqLength, seqNumber, seqType));


        if (!seqType.equals("dna") && !seqType.equals("protein") ||
            seqLength < 0 || seqLength > 500 ||
            seqNumber < 0 || seqNumber > 500) {
            ctx.setVariable("message", "Your input for the form was invalid. Please fill it in again");
            WebConfig.getTemplateEngine().process("sequence_request_form.html", ctx, response.getWriter());
        } else {
            String fragment = seqType;
            ctx.setVariable("fragment", fragment);

            RandomSequenceCreator creator = new RandomSequenceCreator();
            List<String> sequences = creator.createSequences(seqType, seqLength, seqNumber);
            ctx.setVariable("sequences", sequences);
            ctx.setVariable("seq_type", seqType);
            WebConfig.getTemplateEngine().process("sequence_server", ctx, response.getWriter());
        }


//        RequestHistory requestHistory = new RequestHistory();
//        requestHistory.makeList(this.history, seqType, seqLength, seqNumber);
//        System.out.println(this.history);
//        ctx.setVariable("history", this.history);



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        WebConfig.getTemplateEngine().process("sequence_request_form", ctx, response.getWriter());
    }
}
