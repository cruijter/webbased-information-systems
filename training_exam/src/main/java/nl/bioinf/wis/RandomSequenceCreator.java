package nl.bioinf.wis;

import java.util.ArrayList;
import java.util.List;

public class RandomSequenceCreator {
    private static final String[] nucleotides = {"A", "C", "G", "T"};
    private static final String[] aminoAcids = {"A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"};

    public static List<String> createSequences(String seqType, int seqLength, int seqNumber) {
        List<String> sequences = new ArrayList<>();
//        String[] type = null;
        String[] type = nucleotides;

//        if (seqType.equals("dna")) {
//            type = nucleotides;
//        } else
        if (seqType.equals("protein")) {
            type = aminoAcids;
        }

        for (int i = 0; i < seqNumber; i++) {
//            String sequence = "";
            StringBuilder sequence = new StringBuilder();
            for (int j = 0; j < seqLength; j++) {
                String character = type[(int)(Math.random() * type.length)];
//                sequence += character;
                sequence.append(character);
            }
//            sequences.add(sequence);
            sequences.add(sequence.toString());
        }

        return sequences;
    }

    public static void main(String[] args) {
        createSequences("protein", 50, 10);
    }

}
